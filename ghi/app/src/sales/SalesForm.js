import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

function SalesForm () {
  const [autos, setAutos] = useState([]);
  const fetchAutoData = async () => {
		const url = "http://localhost:8100/api/automobiles/"

		const response = await fetch(url);

		if (response.ok) {
				const data = await response.json();
				setAutos(data.autos);
		}
	}

  const [salespeople, setSalespeople] = useState([]);
  const fetchSalespeopleData = async () => {
		const url = "http://localhost:8090/api/salespeople/"

		const response = await fetch(url);

		if (response.ok) {
				const data = await response.json();
				setSalespeople(data.salespeople);
		}
	}

  const [customers, setCustomers] = useState([]);
  const fetchCustomersData = async () => {
		const url = "http://localhost:8090/api/customers/"

		const response = await fetch(url);

		if (response.ok) {
				const data = await response.json();
				setCustomers(data.customers);
		}
	}

  useEffect(() => {
    fetchAutoData()
  }, []);

  useEffect(() => {
    fetchSalespeopleData()
  }, []);

  useEffect(() => {
    fetchCustomersData()
  }, []);

  const [auto, setAuto] = useState('');
	const handleAutoChange = (event) => {
		const value = event.target.value;
		setAuto(value);
	}

  const [salesperson, setSalesperson] = useState('');
	const handleSalespersonChange = (event) => {
		const value = event.target.value;
		setSalesperson(value);
	}

  const [customer, setCustomer] = useState('');
	const handleCustomerChange = (event) => {
		const value = event.target.value;
		setCustomer(value);
	}

  const [price, setPrice] = useState('');
  const handlePriceChange = (event) => {
		const value = event.target.value;
		setPrice(value);
  }

  const navigateTo = useNavigate();

  const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};
		data.automobile = auto;
		data.salesperson = salesperson;
		data.customer = customer;
		data.price = price;
		const salesUrl = 'http://localhost:8090/api/sales/';
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				'Content-type': 'application/json'
			}
		}


    const autoUrl = `http://localhost:8100${auto}sold`
    const fetchAutoConfig = {
      method: 'put',
      headers: {
        'Content-type': 'application/json'
      }
    }

		const response = await fetch(salesUrl, fetchConfig);
		if (response.ok) {
      navigateTo('/sales')
			const autoResponse = await fetch(autoUrl, fetchAutoConfig);
			if (autoResponse.ok) {
				const updatedAuto = await autoResponse.json()
			}
		}
	}

  const unsold = (v) => v.sold !== true;

  return (
    <div>
      <div className="my-2 container">
        <Link to={'..'}>
          <button className="btn bg-2" id="Button">Back to sales list</button>
        </Link>
      </div>
      <div className="shadow p-4 mt-4">
        <h1>Record a new sale</h1>
        <form onSubmit={handleSubmit} >
          <div className="form-floating mb-3">
						<select
						onChange={handleAutoChange}
						value={auto}
						required
						name="auto"
						id="auto"
						className="form-select"
						>
						<option value="">Choose an automobile</option>
							{autos.filter(unsold).map(auto => {
								return (
									<option key={auto.href} value={auto.href}>
										{auto.vin}
									</option>
								);
							})}
						</select>
					</div>
          <div className="form-floating mb-3">
						<select
						onChange={handleSalespersonChange}
						value={salesperson}
						required
						name="salesperson"
						id="salesperson"
						className="form-select"
						>
						<option value="">Choose a salesperson</option>
							{salespeople.map(salesperson => {
								return (
									<option key={salesperson.id} value={salesperson.id}>
										{salesperson.first_name} {salesperson.last_name} - {salesperson.employee_id}
									</option>
								);
							})}
						</select>
					</div>
          <div className="form-floating mb-3">
						<select
						onChange={handleCustomerChange}
						value={customer}
						required
						name="customer"
						id="customer"
						className="form-select"
						>
						<option value="">Choose a customer</option>
							{customers.map(customer => {
								return (
									<option key={customer.id} value={customer.id}>
										{customer.first_name} {customer.last_name}
									</option>
								);
							})}
						</select>
					</div>
          <div className="form-floating mb-3">
            <input
            onChange={handlePriceChange}
            placeholder='price'
            type='text'
            value={price}
						className="form-control"
            />
            <label htmlFor='price'>Price</label>
          </div>
          <button className="btn bg-2" id="Button">Record sale</button>
        </form>
      </div>
    </div>
  )
};

export default SalesForm;
